# 307688_AK_lab3

Энефу Элиша Адуоджо лаб 3 Архитектуры Компьютера

Вариант: asm | acc | harv | hw | tick | struct | stream | mem | prob1

### **Project structure**

Executable files:

`bin/translator.py` - a binary that receives an `.asm` code as input, and outputs a `.out` binary

`bin/run.py` - the virtual machine that will execute the code receives `.out` as input and a `.txt` file with input data


Also:

`machine/isa.py` - instruction set architecture, describes opcodes and instruction architecture

`machine/simulation.py` - contains the virtual machine implementation

`machine/translator.py` - implementation of a translator from assembler to machine code


### **Language description**

Simplified assembly language in extended Backus-Naur form:


- `<letter>` ::= "A" | "B" | "C" | "D" | "E" | "F" | "G"
       | "H" | "I" | "J" | "K" | "L" | "M" | "N"
       | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
       | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
       | "c" | "d" | "e" | "f" | "g" | "h" | "i"
       | "j" | "k" | "l" | "m" | "n" | "o" | "p"
       | "q" | "r" | "s" | "t" | "u" | "v" | "w"
       | "x" | "y" | "z" | "_"

- `<name> ::= <letter> <name> | <letter>`
- `<digit> ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" `
- `<number> ::= <digit> <number> | <digit>`
- `<empty_expr>` ::= "\n"

- `<op_0_arg> ::= "hlt" | "in" | "out"`
- `<op_1_arg> ::= "st" | "ld" | "add" | "mul" | "div" | "mod" | "cmp" | "jmp" | "je" | "word" | "db"`
- `<label_def> ::= <name> ":" | "section .text" | "section .data"`

- `<line> ::= <label_def> | <op_0_arg> | <op_1_arg> " " <label> |  <op_1_arg> " " <label> | <empty_expr>`

- `<program> ::= <line> <program> | <line>`



### **ISA/mnemonics**
Three modes of addressing are supported in the operand:

| mnemonics | number of cycles |
| ------ | ------ |
| `CONSTANT` | the operand is stored directly in the instruction |
| `DIRECT_ADDRESS` | operand is the value at the address stored in the instruction |
| `INDIRECT_ADDRESS` | operand is the value at the address stored in the location pointed to by the instruction |

Command system:


| mnemonics | minimum number of cycles | operand type | description |
| ------ | ------ | ------ | ------ |
| `out` | 2 | `-` | print the value from acc to the output stream |
| `in` | 2 | `-` | read acc value from input stream |
| `hlt` | - | `-` | pause the processor |
| `ld` | 2 | `const/direct_addr/inderect_addr` | load value in acc |
| `st` | 2 | `direct_addr/inderect_addr` | save value from acc to memory |
| `add` | 2 | `const/direct_addr/inderect_addr` | add argument to acc |
| `sub` | 2 | `const/direct_addr/inderect_addr` | subtract argument from acc |
| `mul` | 2 | `const/direct_addr/inderect_addr` | multiply acc by value stream |
| `div` | 2 | `const/direct_addr/inderect_addr` | integer divide acc by value |
| `mod` | 2 | `const/direct_addr/inderect_addr` | get the remainder of dividing acc by the value |
| `cmp` | 2 | `const/direct_addr/inderect_addr` | compare acc with value |
| `jmp` | 2 | `direct_addr` | unconditional transition to the label argument |
| `je` | 2 | `direct_addr` | jump to label argument if equal |


### **TRANSLATOR**

Translates assembly language code into machine instructions (.json)

```
section .data
    curr_char: word 1
    hellow: db "hello world"
    null_term: word 0

section .text
    print:
        ld $curr_char 
        cmp #0 
        je .exit
        out
        ld curr_char
        add #1
        st curr_char
        jmp .print
    exit:
        hlt
```

Result of hello.out translation:

```
{
    "data": [
        1,
        104,
        101,
        108,
        108,
        111,
        32,
        119,
        111,
        114,
        108,
        100,
        0
    ],
    "code": [
        [
            "ld",
            [
                "indirect_address",
                0
            ]
        ],
        [
            "cmp",
            [
                "constant",
                0
            ]
        ],
        [
            "je",
            [
                "constant",
                8
            ]
        ],
        [
            "out",
            null
        ],
        [
            "ld",
            [
                "direct_address",
                0
            ]
        ],
        [
            "add",
            [
                "constant",
                1
            ]
        ],
        [
            "st",
            [
                "constant",
                0
            ]
        ],
        [
            "jmp",
            [
                "constant",
                0
            ]
        ],
        [
            "hlt",
            null
        ]
    ]
}
```

### Model Processor
![Semantic description of image](/img/vm.jpg)

Processor features:

- All operations are built around the acc accumulator, participates in input-output
- 32 bit machine word
- `instr_ptr` - instruction counter, points to the address of the executable instruction
- Implemented Harvard architecture, two separate memory for instructions and for data:
    - instruction memory:
        - `instr_ptr` - instruction counter, points to the address of the instruction being executed
        - `addr_reg` -   to store the address of the cell in the instruction memory (at which address to read / write)
        - `data_reg` - to store a word from data memory or write to data memory


Log of the virtual machine for the program `hello.out`
```
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 1, PC: 0, ADDR: 0, ACC: 0, DR: 1}
{TICK: 2, PC: 0, ADDR: 1, ACC: 0, DR: 104}
{TICK: 3, PC: 1, ADDR: 1, ACC: 104, DR: 104}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 4, PC: 2, ADDR: 1, ACC: 104, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 5, PC: 3, ADDR: 1, ACC: 104, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'h' will be added to ''}
{TICK: 6, PC: 3, ADDR: 1, ACC: 104, DR: 0}
{TICK: 7, PC: 4, ADDR: 1, ACC: 104, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 8, PC: 4, ADDR: 0, ACC: 104, DR: 1}
{TICK: 9, PC: 5, ADDR: 0, ACC: 1, DR: 1}
{starting executing: ['add', ['constant', 1]]}
{TICK: 10, PC: 5, ADDR: 0, ACC: 2, DR: 1}
{TICK: 11, PC: 6, ADDR: 0, ACC: 2, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 12, PC: 7, ADDR: 0, ACC: 2, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 13, PC: 0, ADDR: 0, ACC: 2, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 14, PC: 0, ADDR: 0, ACC: 2, DR: 2}
{TICK: 15, PC: 0, ADDR: 2, ACC: 2, DR: 101}
{TICK: 16, PC: 1, ADDR: 2, ACC: 101, DR: 101}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 17, PC: 2, ADDR: 2, ACC: 101, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 18, PC: 3, ADDR: 2, ACC: 101, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'e' will be added to 'h'}
{TICK: 19, PC: 3, ADDR: 2, ACC: 101, DR: 0}
{TICK: 20, PC: 4, ADDR: 2, ACC: 101, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 21, PC: 4, ADDR: 0, ACC: 101, DR: 2}
{TICK: 22, PC: 5, ADDR: 0, ACC: 2, DR: 2}
{starting executing: ['add', ['constant', 1]]}
{TICK: 23, PC: 5, ADDR: 0, ACC: 3, DR: 1}
{TICK: 24, PC: 6, ADDR: 0, ACC: 3, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 25, PC: 7, ADDR: 0, ACC: 3, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 26, PC: 0, ADDR: 0, ACC: 3, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 27, PC: 0, ADDR: 0, ACC: 3, DR: 3}
{TICK: 28, PC: 0, ADDR: 3, ACC: 3, DR: 108}
{TICK: 29, PC: 1, ADDR: 3, ACC: 108, DR: 108}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 30, PC: 2, ADDR: 3, ACC: 108, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 31, PC: 3, ADDR: 3, ACC: 108, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'l' will be added to 'he'}
{TICK: 32, PC: 3, ADDR: 3, ACC: 108, DR: 0}
{TICK: 33, PC: 4, ADDR: 3, ACC: 108, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 34, PC: 4, ADDR: 0, ACC: 108, DR: 3}
{TICK: 35, PC: 5, ADDR: 0, ACC: 3, DR: 3}
{starting executing: ['add', ['constant', 1]]}
{TICK: 36, PC: 5, ADDR: 0, ACC: 4, DR: 1}
{TICK: 37, PC: 6, ADDR: 0, ACC: 4, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 38, PC: 7, ADDR: 0, ACC: 4, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 39, PC: 0, ADDR: 0, ACC: 4, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 40, PC: 0, ADDR: 0, ACC: 4, DR: 4}
{TICK: 41, PC: 0, ADDR: 4, ACC: 4, DR: 108}
{TICK: 42, PC: 1, ADDR: 4, ACC: 108, DR: 108}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 43, PC: 2, ADDR: 4, ACC: 108, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 44, PC: 3, ADDR: 4, ACC: 108, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'l' will be added to 'hel'}
{TICK: 45, PC: 3, ADDR: 4, ACC: 108, DR: 0}
{TICK: 46, PC: 4, ADDR: 4, ACC: 108, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 47, PC: 4, ADDR: 0, ACC: 108, DR: 4}
{TICK: 48, PC: 5, ADDR: 0, ACC: 4, DR: 4}
{starting executing: ['add', ['constant', 1]]}
{TICK: 49, PC: 5, ADDR: 0, ACC: 5, DR: 1}
{TICK: 50, PC: 6, ADDR: 0, ACC: 5, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 51, PC: 7, ADDR: 0, ACC: 5, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 52, PC: 0, ADDR: 0, ACC: 5, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 53, PC: 0, ADDR: 0, ACC: 5, DR: 5}
{TICK: 54, PC: 0, ADDR: 5, ACC: 5, DR: 111}
{TICK: 55, PC: 1, ADDR: 5, ACC: 111, DR: 111}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 56, PC: 2, ADDR: 5, ACC: 111, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 57, PC: 3, ADDR: 5, ACC: 111, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'o' will be added to 'hell'}
{TICK: 58, PC: 3, ADDR: 5, ACC: 111, DR: 0}
{TICK: 59, PC: 4, ADDR: 5, ACC: 111, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 60, PC: 4, ADDR: 0, ACC: 111, DR: 5}
{TICK: 61, PC: 5, ADDR: 0, ACC: 5, DR: 5}
{starting executing: ['add', ['constant', 1]]}
{TICK: 62, PC: 5, ADDR: 0, ACC: 6, DR: 1}
{TICK: 63, PC: 6, ADDR: 0, ACC: 6, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 64, PC: 7, ADDR: 0, ACC: 6, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 65, PC: 0, ADDR: 0, ACC: 6, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 66, PC: 0, ADDR: 0, ACC: 6, DR: 6}
{TICK: 67, PC: 0, ADDR: 6, ACC: 6, DR: 32}
{TICK: 68, PC: 1, ADDR: 6, ACC: 32, DR: 32}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 69, PC: 2, ADDR: 6, ACC: 32, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 70, PC: 3, ADDR: 6, ACC: 32, DR: 0}
{starting executing: ['out', None]}
{new output symbol ' ' will be added to 'hello'}
{TICK: 71, PC: 3, ADDR: 6, ACC: 32, DR: 0}
{TICK: 72, PC: 4, ADDR: 6, ACC: 32, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 73, PC: 4, ADDR: 0, ACC: 32, DR: 6}
{TICK: 74, PC: 5, ADDR: 0, ACC: 6, DR: 6}
{starting executing: ['add', ['constant', 1]]}
{TICK: 75, PC: 5, ADDR: 0, ACC: 7, DR: 1}
{TICK: 76, PC: 6, ADDR: 0, ACC: 7, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 77, PC: 7, ADDR: 0, ACC: 7, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 78, PC: 0, ADDR: 0, ACC: 7, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 79, PC: 0, ADDR: 0, ACC: 7, DR: 7}
{TICK: 80, PC: 0, ADDR: 7, ACC: 7, DR: 119}
{TICK: 81, PC: 1, ADDR: 7, ACC: 119, DR: 119}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 82, PC: 2, ADDR: 7, ACC: 119, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 83, PC: 3, ADDR: 7, ACC: 119, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'w' will be added to 'hello '}
{TICK: 84, PC: 3, ADDR: 7, ACC: 119, DR: 0}
{TICK: 85, PC: 4, ADDR: 7, ACC: 119, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 86, PC: 4, ADDR: 0, ACC: 119, DR: 7}
{TICK: 87, PC: 5, ADDR: 0, ACC: 7, DR: 7}
{starting executing: ['add', ['constant', 1]]}
{TICK: 88, PC: 5, ADDR: 0, ACC: 8, DR: 1}
{TICK: 89, PC: 6, ADDR: 0, ACC: 8, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 90, PC: 7, ADDR: 0, ACC: 8, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 91, PC: 0, ADDR: 0, ACC: 8, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 92, PC: 0, ADDR: 0, ACC: 8, DR: 8}
{TICK: 93, PC: 0, ADDR: 8, ACC: 8, DR: 111}
{TICK: 94, PC: 1, ADDR: 8, ACC: 111, DR: 111}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 95, PC: 2, ADDR: 8, ACC: 111, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 96, PC: 3, ADDR: 8, ACC: 111, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'o' will be added to 'hello w'}
{TICK: 97, PC: 3, ADDR: 8, ACC: 111, DR: 0}
{TICK: 98, PC: 4, ADDR: 8, ACC: 111, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 99, PC: 4, ADDR: 0, ACC: 111, DR: 8}
{TICK: 100, PC: 5, ADDR: 0, ACC: 8, DR: 8}
{starting executing: ['add', ['constant', 1]]}
{TICK: 101, PC: 5, ADDR: 0, ACC: 9, DR: 1}
{TICK: 102, PC: 6, ADDR: 0, ACC: 9, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 103, PC: 7, ADDR: 0, ACC: 9, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 104, PC: 0, ADDR: 0, ACC: 9, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 105, PC: 0, ADDR: 0, ACC: 9, DR: 9}
{TICK: 106, PC: 0, ADDR: 9, ACC: 9, DR: 114}
{TICK: 107, PC: 1, ADDR: 9, ACC: 114, DR: 114}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 108, PC: 2, ADDR: 9, ACC: 114, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 109, PC: 3, ADDR: 9, ACC: 114, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'r' will be added to 'hello wo'}
{TICK: 110, PC: 3, ADDR: 9, ACC: 114, DR: 0}
{TICK: 111, PC: 4, ADDR: 9, ACC: 114, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 112, PC: 4, ADDR: 0, ACC: 114, DR: 9}
{TICK: 113, PC: 5, ADDR: 0, ACC: 9, DR: 9}
{starting executing: ['add', ['constant', 1]]}
{TICK: 114, PC: 5, ADDR: 0, ACC: 10, DR: 1}
{TICK: 115, PC: 6, ADDR: 0, ACC: 10, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 116, PC: 7, ADDR: 0, ACC: 10, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 117, PC: 0, ADDR: 0, ACC: 10, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 118, PC: 0, ADDR: 0, ACC: 10, DR: 10}
{TICK: 119, PC: 0, ADDR: 10, ACC: 10, DR: 108}
{TICK: 120, PC: 1, ADDR: 10, ACC: 108, DR: 108}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 121, PC: 2, ADDR: 10, ACC: 108, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 122, PC: 3, ADDR: 10, ACC: 108, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'l' will be added to 'hello wor'}
{TICK: 123, PC: 3, ADDR: 10, ACC: 108, DR: 0}
{TICK: 124, PC: 4, ADDR: 10, ACC: 108, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 125, PC: 4, ADDR: 0, ACC: 108, DR: 10}
{TICK: 126, PC: 5, ADDR: 0, ACC: 10, DR: 10}
{starting executing: ['add', ['constant', 1]]}
{TICK: 127, PC: 5, ADDR: 0, ACC: 11, DR: 1}
{TICK: 128, PC: 6, ADDR: 0, ACC: 11, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 129, PC: 7, ADDR: 0, ACC: 11, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 130, PC: 0, ADDR: 0, ACC: 11, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 131, PC: 0, ADDR: 0, ACC: 11, DR: 11}
{TICK: 132, PC: 0, ADDR: 11, ACC: 11, DR: 100}
{TICK: 133, PC: 1, ADDR: 11, ACC: 100, DR: 100}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 134, PC: 2, ADDR: 11, ACC: 100, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 135, PC: 3, ADDR: 11, ACC: 100, DR: 0}
{starting executing: ['out', None]}
{new output symbol 'd' will be added to 'hello worl'}
{TICK: 136, PC: 3, ADDR: 11, ACC: 100, DR: 0}
{TICK: 137, PC: 4, ADDR: 11, ACC: 100, DR: 0}
{starting executing: ['ld', ['direct_address', 0]]}
{TICK: 138, PC: 4, ADDR: 0, ACC: 100, DR: 11}
{TICK: 139, PC: 5, ADDR: 0, ACC: 11, DR: 11}
{starting executing: ['add', ['constant', 1]]}
{TICK: 140, PC: 5, ADDR: 0, ACC: 12, DR: 1}
{TICK: 141, PC: 6, ADDR: 0, ACC: 12, DR: 1}
{starting executing: ['st', ['constant', 0]]}
{TICK: 142, PC: 7, ADDR: 0, ACC: 12, DR: 0}
{starting executing: ['jmp', ['constant', 0]]}
{TICK: 143, PC: 0, ADDR: 0, ACC: 12, DR: 0}
{starting executing: ['ld', ['indirect_address', 0]]}
{TICK: 144, PC: 0, ADDR: 0, ACC: 12, DR: 12}
{TICK: 145, PC: 0, ADDR: 12, ACC: 12, DR: 0}
{TICK: 146, PC: 1, ADDR: 12, ACC: 0, DR: 0}
{starting executing: ['cmp', ['constant', 0]]}
{TICK: 147, PC: 2, ADDR: 12, ACC: 0, DR: 0}
{starting executing: ['je', ['constant', 8]]}
{TICK: 148, PC: 8, ADDR: 12, ACC: 0, DR: 8}
{starting executing: ['hlt', None]}
simulation finished
output: 'hello world', instructions: '91', ticks: '148'
```

### **Approbation**

| ФИО | Algorithm | LoC | code byte | code instr | instr. | cycles |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Enefu Elisha | hello | 18 | - | 9 | 91 | 148 |
| Enefu Elisha | cat | 13 | - | 6 | 35 | 56 |
| Enefu Elisha | prob1 | - | 17 | 799 | 1345 |
