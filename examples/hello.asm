section .data
    char: word 1
    hellow: db "hello, I am Elisha"
    null: word 0

section .text
    print:
        ld $char
        cmp #0
        je .exit
        out
        ld char
        add #1
        st char
        jmp .print
    exit:
        hlt
