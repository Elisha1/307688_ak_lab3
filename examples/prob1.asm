section .data
	limit: word 1000
	sum_of_3: word 0
	sum_of_5: word 0
	sum_of_15: word 0
	floor: word 0
	total: word 0

section .text
	find_sum_of_3: 	ld limit
					sub #1
					div #3
					st floor
					add #1
					div #2
					mul floor
					mul #3
					st sum_of_3
	find_sum_of_5:	ld limit
					sub #1
					div #5
					st floor
					add #1
					div #2
					mul floor
					mul #5
					st sum_of_5
	find_sum_of_15:	ld limit
					sub #1
					div #15
					st floor
					add #1
					div #2
					mul floor
					mul #15
					st sum_of_15
	find_total_sum:	ld sum_of_3
					add sum_of_5
					sub sum_of_15
					st total
	finish:	hlt
