section .data
    null: word 0

section .text
    read_char:
        in
        cmp null
        je .exit
        out
        jmp .read_char
    exit:
        hlt
